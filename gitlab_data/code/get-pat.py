#! /usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import sys


api_key_name = sys.arg[1]
gitlab_URL = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

# instantiate a chrome options object so you can set the size and headless preference
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=1920x1080")

# download the chrome driver from https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in the
# current directory
# on Mac OS X(what's used here..) you can use brew: `brew cask install chromedriver`

chrome_driver = "/usr/bin/chromedriver"

driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=chrome_driver)

driver.get(f"{gitlab_URL}/profile/personal_access_tokens")

driver.find_element_by_id("user_login").send_keys(f"{username}")
driver.find_element_by_id ("user_password").send_keys(f"{password}")
driver.find_element_by_name("commit").click()

driver.find_element_by_id("personal_access_token_name").send_keys(f"{api_key_name}")
driver.find_element_by_xpath("//form[@id='new_personal_access_token']/div[3]/fieldset").click()
driver.find_element_by_id("personal_access_token_scopes_api").click()
driver.find_element_by_xpath("//form[@id='new_personal_access_token']/div[3]/fieldset[2]").click()
driver.find_element_by_id("personal_access_token_scopes_read_user").click()
driver.find_element_by_id("personal_access_token_scopes_read_repository").click()
driver.find_element_by_id("personal_access_token_scopes_write_repository").click()
driver.find_element_by_id("personal_access_token_scopes_sudo").click()
driver.find_element_by_name("commit").click()
token = driver.find_element_by_name("created-personal-access-token").get_attribute('value')
print(token)

# #driver.find_element_by_id(".form-group:nth-child(2)").click()

# capture the screen
driver.get_screenshot_as_file("capture1235.png")
