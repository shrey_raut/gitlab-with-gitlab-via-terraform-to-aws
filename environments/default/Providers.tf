provider "aws" {
  version = "~> 2.8"
  region = var.region
}

provider "random" {
  version = "~> 2.1"
}
